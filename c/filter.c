#include "../h/filter.h"

void filterSharpen(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
	int row, col, sumR, sumG, sumB, i, j;
	float boxsharpen[3][3] = {
		{ 0, -1, 0 },
		{ -1, 5, -1 },
		{ 0, -1, 0 }
	};

	for(row = 0; row < n; row++){
		for(col = 0; col < m; col++){
			sumR = 0;
			sumG = 0;
			sumB = 0;

			for(i = 0; i < 3; i++){
				for(j = 0; j < 3; j++){
					sumR += original[row + i][col + j].r * boxsharpen[i][j];
					sumG += original[row + i][col + j].g * boxsharpen[i][j];
					sumB += original[row + i][col + j].b * boxsharpen[i][j];
				}
			}

			if(sumR > 255) sumR = 255;
			if(sumR < 0) sumR = 0;
			if(sumG > 255) sumG = 255;
			if(sumG < 0) sumG = 0;
			if(sumB > 255) sumB = 255;
			if(sumB < 0) sumB = 0;
			
            Pixel temp;
            temp.r = sumR;
            temp.g = sumG;
            temp.b = sumB;

			modified[row][col] = temp;
		}
	}
}

void filterBlur(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
	int row, col, sumR, sumG, sumB, i, j;
	float boxblur[3][3] = {
		{ 0.0625, 0.125, 0.0625 },
		{ 0.125, 0.25, 0.125 },
		{ 0.0625, 0.125, 0.0625 }
	};

	for(row = 0; row < n; row++){
		for(col = 0; col < m; col++){
			sumR = 0;
			sumG = 0;
			sumB = 0;

			for(i = 0; i < 3; i++){
				for(j = 0; j < 3; j++){
					sumR += original[row + i][col + j].r * boxblur[i][j];
					sumG += original[row + i][col + j].g * boxblur[i][j];
					sumB += original[row + i][col + j].b * boxblur[i][j];
				}
			}

			if(sumR > 255) sumR = 255;
			if(sumR < 0) sumR = 0;
			if(sumG > 255) sumG = 255;
			if(sumG < 0) sumG = 0;
			if(sumB > 255) sumB = 255;
			if(sumB < 0) sumB = 0;
			
            Pixel temp;
            temp.r = sumR;
            temp.g = sumG;
            temp.b = sumB;

			modified[row][col] = temp;
		}
	}
}

void filterEmboss(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
	int row, col, sumR, sumG, sumB, i, j;
	float boxemboss[3][3] = {
		{ -2, -1, 0 },
		{ -1, 1, 1 },
		{ 0, 1, 2 }
	};

	for(row = 0; row < n; row++){
		for(col = 0; col < m; col++){
			sumR = 0;
			sumG = 0;
			sumB = 0;

			for(i = 0; i < 3; i++){
				for(j = 0; j < 3; j++){
					sumR += original[row + i][col + j].r * boxemboss[i][j];
					sumG += original[row + i][col + j].g * boxemboss[i][j];
					sumB += original[row + i][col + j].b * boxemboss[i][j];
				}
			}

			if(sumR > 255) sumR = 255;
			if(sumR < 0) sumR = 0;
			if(sumG > 255) sumG = 255;
			if(sumG < 0) sumG = 0;
			if(sumB > 255) sumB = 255;
			if(sumB < 0) sumB = 0;
			
            Pixel temp;
            temp.r = sumR;
            temp.g = sumG;
            temp.b = sumB;

			modified[row][col] = temp;
		}
	}
}

void filterIdentity(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
	int row, col, sumR, sumG, sumB, i, j;
	float boxidentity[3][3] = {
		{ 0, 0, 0 },
		{ 0, 1, 0 },
		{ 0, 0, 0 }
	};

	for(row = 0; row < n; row++){
		for(col = 0; col < m; col++){
			sumR = 0;
			sumG = 0;
			sumB = 0;

			for(i = 0; i < 3; i++){
				for(j = 0; j < 3; j++){
					sumR += original[row + i][col + j].r * boxidentity[i][j];
					sumG += original[row + i][col + j].g * boxidentity[i][j];
					sumB += original[row + i][col + j].b * boxidentity[i][j];
				}
			}

			if(sumR > 255) sumR = 255;
			if(sumR < 0) sumR = 0;
			if(sumG > 255) sumG = 255;
			if(sumG < 0) sumG = 0;
			if(sumB > 255) sumB = 255;
			if(sumB < 0) sumB = 0;
			
            Pixel temp;
            temp.r = sumR;
            temp.g = sumG;
            temp.b = sumB;

			modified[row][col] = temp;
		}
	}
}

void filterGrayscale(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
	int row, col, average;

	for(row = 0; row < n; row++){
		for(col = 0; col < m; col++){
		    Pixel temp = original[row][col];
		    average = (temp.r + temp.g + temp.b)/3;
		    temp.r = average;
		    temp.g = average;
		    temp.b = average;
            modified[row][col] = temp;
		}
	}
}

void filterThresholding(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
    int row, col, average;
    
    for(row = 0; row < n; row++){
        for(col = 0; col < m; col++){
            Pixel temp = original[row][col];
            average = (temp.r + temp.g + temp.b)/3;
            
            if(average > 128){
                average = 255;
            }
            
            else{
                average = 0;
            }
            
            temp.r = average;
		    temp.g = average;
		    temp.b = average;
            modified[row][col] = temp;
        }
    }
}

void filterRotate(int n, int m, Pixel original[n][m], Pixel modified[n][m]){
    int row, col;
    
    for(row = 0; row < n; row++){
        for(col = 0; col < m; col++){
            Pixel temp = original[row][col];
            modified[n-row-1][m-col-1] = temp;
        }
    }
}