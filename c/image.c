#include <stdio.h>
#include "../h/image.h"
#include "../h/filter.h"

void readImage(int row, int col, Pixel originalImage[row][col]){
	int i, j;

	for(i = 0; i < row; i++){
		for(j = 0; j < col; j++){
			Pixel temp;
			scanf("%hhu %hhu %hhu", &temp.r, &temp.g, &temp.b);

			originalImage[i][j] = temp;
		}
	}
}

void generateImage(int row, int col, Pixel image[row][col]){
	int i, j;

	for(i = 0; i < row; i++){
		for(j = 0; j < col; j++){
			printf("%hhu %hhu %hhu\n", image[i][j].r, image[i][j].g, image[i][j].b);
		}
	}
}

void filterImage(short filter, int row, int col, Pixel originalImage[row][col], Pixel modified[row][col]){
    switch(filter){
        case SHARPEN:
            filterSharpen(row, col, originalImage, modified);
            break;
        
        case BLUR:
            filterBlur(row, col, originalImage, modified);
            break;
        
        case EMBOSS:
            filterEmboss(row, col, originalImage, modified);
            break;
        
        case IDENTITY:
            filterIdentity(row, col, originalImage, modified);
            break;
        
        case GRAYSCALE:
            filterGrayscale(row, col, originalImage, modified);
            break;
        
        case THRESHOLDING:
            filterThresholding(row, col, originalImage, modified);
            break;
        
        case ROTATE:
            filterRotate(row, col, originalImage, modified);
            break;
    }
}
