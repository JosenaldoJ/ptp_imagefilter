#include <stdio.h>
#include "../h/image.h"
#include "../h/filter.h"

int main(){
	char type[2];
	int i, j, col, row, qual;

	scanf("%s", type);
	scanf("%i %i", &row, &col);
	scanf("%i", &qual);

	printf("%s\n", type);
	printf("%i %i\n", row, col);
	printf("%i\n", qual);

	Pixel originalImage[row][col];
	Pixel modified[row][col];

	readImage(row, col, originalImage);
	filterImage(EMBOSS, row, col, originalImage, modified);
	generateImage(row, col, modified);

	return 0;
}


