#ifndef FILTER_H
#define FILTER_H

#include "image.h"

typedef enum enumFilter{
    SHARPEN,
    BLUR, 
    EMBOSS,
    IDENTITY,
    GRAYSCALE,
    THRESHOLDING,
    ROTATE
} Filter;

void filterSharpen(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
void filterBlur(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
void filterEmboss(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
void filterIdentity(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
void filterGrayscale(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
void filterThresholding(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
void filterRotate(int n, int m, Pixel original[n][m], Pixel modified[n][m]);
    
#endif
