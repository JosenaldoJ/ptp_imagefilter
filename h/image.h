#ifndef IMAGE_H
#define IMAGE_H

typedef struct strPixel{
	unsigned char r;
	unsigned char g;
	unsigned char b;
} Pixel;

void readImage(int row, int col, Pixel originalImage[row][col]);
void generateImage(int row, int col, Pixel image[row][col]);
void filterImage(short filter, int row, int col, Pixel originalImage[row][col], Pixel modified[row][col]);

#endif